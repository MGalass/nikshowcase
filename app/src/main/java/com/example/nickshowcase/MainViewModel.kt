package com.example.nickshowcase

import android.content.Context
import android.graphics.Color
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt

class MainViewModel(val context: Context, val listener: MainViewModelListener) {

    /**
     * Interfaccia che usiamo per gestire le operazioni che il ViewModel richiede all'Activity
     */
    interface MainViewModelListener {

    }

    /**
     * Questa variabile observable sarà il nostro counter. Viene referenziata direttamente in activity_main dentro la text property.
     * Qualunque modifica a questa variabile verrà propagata alla view
     */
    val counter = ObservableInt(0)
    val showCounter = ObservableBoolean(true)
    val currentColor = ObservableInt(Color.BLACK)

    /**
     * Questo metodo è richiamato direttamente in activity_main tramite l'onClick property
     */
    fun updateCounter() {
        counter.set(counter.get() + 1)
    }

    fun toggleVisibility() {
        showCounter.set(!showCounter.get())
    }

    fun changeColors() {
        val rnd = java.util.Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        currentColor.set(color)
    }

}