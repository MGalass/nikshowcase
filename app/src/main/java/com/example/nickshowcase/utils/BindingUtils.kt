package com.example.nickshowcase.utils

import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.example.nickshowcase.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

object BindingUtils {

    /**
     * La nuova proprietà avrà come nome specificato nel "value" nell'annotation.
     * Il primo parametro è sempre la view sulla quale la property viene applicata.
     * A seguire i parametri. Se ci sono due o più parametri la sintassi è leggermente diversa.
     * https://developer.android.com/topic/libraries/data-binding/binding-adapters#kotlin
     */
    @JvmStatic
    @BindingAdapter("fabDrawable")
    fun setFABDrawable(fab: FloatingActionButton, isVisible: Boolean) {
        fab.setImageDrawable(ContextCompat.getDrawable(fab.context, if (isVisible) R.drawable.ic_visibility_off_white_24dp else R.drawable.ic_visibility_white_24dp));
    }
}