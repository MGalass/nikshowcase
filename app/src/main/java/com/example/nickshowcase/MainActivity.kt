package com.example.nickshowcase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.nickshowcase.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), MainViewModel.MainViewModelListener{

    /**
     * La classe ActivityMainBinding viene generata automaticamente in fase di building.
     * Si tratta di una classe POJO che permette di referenziare qualsiasi View nel layout dotata di id come se fosse un semplice campo
     */
    var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Usiamo le utils della Databinding library
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // Settiamo il viewModel
        binding?.let {
            it.viewModel = MainViewModel(this, this)
        }
    }
}
